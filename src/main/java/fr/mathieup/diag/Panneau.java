package fr.mathieup.diag;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

public class Panneau extends JPanel {

    private Scrollbar scrollbar = new Scrollbar(Scrollbar.VERTICAL, 0, 1, 0, 255);
    private int value = 0;
    public Panneau(){
        this.setDropTarget(new DropTarget() {
            public synchronized void drop(DropTargetDropEvent evt) {
                try {
                    evt.acceptDrop(DnDConstants.ACTION_COPY);
                    java.util.List<File> droppedFiles = (List<File>)
                            evt.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                    if (droppedFiles.size() > 0) {
                        Main.selectedFile = droppedFiles.get(0);
                        Main.getHexa();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        this.setLayout(new BorderLayout());
        this.add(scrollbar, BorderLayout.EAST);

        int line = Main.hexa.size()/16;
        if (line > 16){
            scrollbar.setMaximum(line-16);
        } else {
            scrollbar.setMaximum(0);
        }
        scrollbar.repaint();
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (value != scrollbar.getValue() && Main.fen != null && Main.fen.getGraphics() != null) {
                    value = scrollbar.getValue();
                    Thread t = new Thread(() -> Main.fen.update(Main.fen.getGraphics()));
                    if (SwingUtilities.isEventDispatchThread())
                        t.start();
                    else {
                        SwingUtilities.invokeLater(t);
                    }
                }
            }
        }).start();
    }

    public Scrollbar getScrollbar() {
        return scrollbar;
    }

    public void paintComponent(Graphics g) {
        g.setColor(Color.BLACK);
        int sizeLeft = 50;
        int nb = 16;
        int statescroll = scrollbar.getValue() + 1;
        g.drawRect(sizeLeft+nb*20, 0, 5, getHeight());
        for(int i = 0; i < nb; i++){
            g.drawString(String.valueOf(i), sizeLeft+i*20, 20);
        }
        int c = 0;
        for (String s : Main.hexa.stream().map(e -> Integer.toHexString(e).toUpperCase()).collect(Collectors.toList())) {
            int h = (c/nb)*20+50-statescroll*20;
            if (h >= 50 && h <= 630) {
                g.drawString(s.length() == 1 ? "0" + s : s, ((c % nb) * 20) + sizeLeft, h);
                if (c % nb == 0) {
                    String s2 = Integer.toHexString(c / nb).toUpperCase()+"0";
                    while (s2.length() < 4){
                        s2 = "0" + s2;
                    }
                    g.drawString(s2, 5, h);
                }
            }
            c++;
        }
        c = 0;
        for (String s : Main.hexa.stream().map(e -> ((char) e.intValue())+"").collect(Collectors.toList())) {
            int h = (c/nb)*20+50-statescroll*20;
            if (h >= 50 && h <= 630)
                g.drawString(String.valueOf((char) 0).equals(s) ? "." : s, sizeLeft+nb*20+20+(c%nb)*12, h);
            c++;
        }
    }
}
