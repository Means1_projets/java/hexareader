package fr.mathieup.diag;

import javax.swing.*;

import java.awt.*;
import java.util.stream.Collectors;

public class Fenetre extends JFrame {
    Panneau panneau;
    private JMenuBar menuBar = new JMenuBar();
    private JMenu file = new JMenu("Fichier");
    private JMenu decod = new JMenu("Décodeur");
    private JMenuItem open = new JMenuItem("Ouvrir");
    private JMenuItem save = new JMenuItem("Save");
    private JMenuItem saveas = new JMenuItem("Save Vers");
    private JMenuItem inversion = new JMenuItem("Inversion");
    private JMenuItem plus1 = new JMenuItem("+1");
    private JMenuItem moins1 = new JMenuItem("-1");
    private JMenuItem binary = new JMenuItem("Binaire");
    private JMenuItem close = new JMenuItem("Fermer");

    private JMenuItem search = new JMenuItem("Rechercher");

    public Fenetre(){
        this.setTitle("Le décodeur");
        this.setSize(625, 600);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.open.addActionListener(e -> Main.openSelectFile());
        this.saveas.addActionListener(e -> Main.saveFile());
        this.save.addActionListener(e -> Main.saveHexa(Main.selectedFile));
        this.file.add(this.open);
        this.file.add(this.save);
        this.file.add(this.saveas);
        this.file.add(this.close);
        this.decod.add(this.inversion);
        this.decod.add(this.plus1);
        this.decod.add(this.moins1);
        this.decod.add(this.binary);
        this.menuBar.add(this.file);
        this.menuBar.add(this.decod);
        this.inversion.addActionListener(e -> {
            Main.hexa = Main.hexa.stream().map(f -> 255-f).collect(Collectors.toList());
            Main.fen.update(Main.fen.getGraphics());
        });
        this.plus1.addActionListener(e -> {
            Main.hexa = Main.hexa.stream().map(f -> f+1).collect(Collectors.toList());
            Main.fen.update(Main.fen.getGraphics());
        });
        this.moins1.addActionListener(e -> {
            Main.hexa = Main.hexa.stream().map(f -> f-1).collect(Collectors.toList());
            Main.fen.update(Main.fen.getGraphics());
        });
        this.binary.addActionListener(e -> {

            String te = Main.hexa.stream().map(Integer::toBinaryString).reduce((p, q) -> p+q).orElse("");

            JTextArea textArea = new JTextArea(te);
            textArea.setFont(new Font("Serif", Font.ITALIC, 16));
            textArea.setLineWrap(true);
            textArea.setWrapStyleWord(true);
            textArea.setEditable(false);
            JScrollPane areaScrollPane = new JScrollPane(textArea);
            areaScrollPane.setVerticalScrollBarPolicy(
                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            areaScrollPane.setBorder(
                    BorderFactory.createCompoundBorder(
                            BorderFactory.createCompoundBorder(
                                    BorderFactory.createTitledBorder("Code Binaire"),
                                    BorderFactory.createEmptyBorder(5,5,5,5)),
                            areaScrollPane.getBorder()));
            JDialog dialog = new JDialog(this, "test", true);
            dialog.setSize(500, 500);
            dialog.add(areaScrollPane,
                    BorderLayout.CENTER);
            dialog.setVisible(true);
        });
        this.close.addActionListener(e -> System.exit(0));
        this.setJMenuBar(this.menuBar);
        this.setContentPane(this.panneau = new Panneau());
        this.setVisible(true);
    }

    public Panneau getPanneau() {
        return panneau;
    }
}