package fr.mathieup.diag;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static List<Integer> hexa = new ArrayList<>();
    public static Fenetre fen;
    public static File selectedFile;

    public static void openSelectFile(){
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("BIN Datas", "bin");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            selectedFile = chooser.getSelectedFile();
            getHexa();
        }
    }

    public static void getHexa(){
        hexa.clear();
        try {
            FileInputStream a = new FileInputStream(selectedFile);
            int b;
            while ((b = a.read()) != -1) {
                hexa.add(b);
            }
            a.close();

            int line = Main.hexa.size()/16;
            if (line > 16){
                fen.getPanneau().getScrollbar().setMaximum(line-16);
            } else {
                fen.getPanneau().getScrollbar().setMaximum(0);
            }
            fen.getPanneau().getScrollbar().repaint();
            fen.update(fen.getGraphics());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        fen = new Fenetre();
    }

    public static void saveFile() {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("BIN Datas", "bin");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            saveHexa(chooser.getSelectedFile());
        }
    }

    static void saveHexa(File selectedFile) {
        try {
            FileOutputStream a = new FileOutputStream(selectedFile);
            hexa.forEach(b -> {
                try {
                    a.write(b);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            a.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
